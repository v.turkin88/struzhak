FactoryBot.define do
  factory :privacy_policy do
    abc = ("a".."z").to_a.join

    sequence(:title) { |n| "Privacy Policy #{n}" }
    sequence(:body) { |n| "body #{abc[n]}" }
    locale_id { (create :locale, active: true).id }
    document_url { 'https://gist.github.com/mlanett/a31c340b132ddefa9cca' }
  end
end

FactoryBot.define do
  factory :email_sender do
    sequence(:address) { 'smtp.gmail.com' }
    sequence(:port) { 587 }
    sequence(:domain) { 'dom.com' }
    sequence(:authentication) { 'plain' }
    sequence(:user_name) { 'user@gmail.com' }
    sequence(:password) { 'password' }
    sequence(:enable_starttls_auto) { true }
  end
end

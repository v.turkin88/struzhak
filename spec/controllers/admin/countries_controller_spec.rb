# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Admin::CountriesController, type: :controller do
  render_views

  let(:admin) { create :user, :admin }
  let(:business) { create :user, :business }
  let(:country) { create :country }

  describe 'with success' do
    let(:response_body) { JSON.parse(response.body) }

    let(:response_body) { JSON.parse(response.body) }

    let(:country_params) do
      {
        country: {
          selected: false,
          phone_code: '+123',
          name: 'TEST',
          alpha2_code: 'AF',
          alpha3_code: 'AFG',
          numeric_code: '123'
        }
      }
    end

    it 'for create' do
      sign_in user: admin

      post :create, params: country_params

      expect(response.status).to be 200
    end

    it 'for update' do
      sign_in user: admin
      put :update, params: { country: { selected: true}, id: country.id }

      expect(response.status).to be 200
      expect(Country.find(country.id).selected).to be true
    end

    it 'for destroy' do
      sign_in user: admin

      country = create :country

      delete :destroy, params: {id: country.id}

      expect(response.status).to be 200
    end

    it 'for show' do
      sign_in user: admin

      country = create :country

      get :show, params: { id: country.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['country']['name']).to eq(country.name)
      expect(JSON.parse(response.body)['country']['phone_code']).to eq(country.phone_code)
    end

    it 'for index' do
      sign_in user: admin

      create :country, name: 'Country 1'
      create :country, name: 'Country 2'

      get :index

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['countries'].count).to eq(2)
      expect(JSON.parse(response.body)['count']).to eq(2)
    end

    it 'for index by name' do
      sign_in user: admin

      create :country, name: 'Country 1'
      create :country, name: 'Country 2'

      get :index, params: { name: country.name }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['countries'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(1)
    end

    it 'for index paginate' do
      sign_in user: admin

      get :index, params: { per_page: 1, page: 1, sort_column: 'name',
                            sort_type: 'asc', name: 'test', alpha2_code: 'GG',
                            alpha3_code: 'GGG', numeric_code: '123', phone_code: '111' }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['countries'].count).to eq(0)
      expect(JSON.parse(response.body)['count']).to eq(0)
    end
  end

  describe 'for error' do
    it 'for show' do
      get :show, params: { id: -100 }

      expect(response.status).to be 400
    end

    it 'for show' do
      sign_in user: business
      get :show, params: { id: -100 }

      expect(response.status).to be 401
    end
  end
end

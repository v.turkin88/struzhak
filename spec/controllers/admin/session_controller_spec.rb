# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::SessionsController, type: :controller do
  render_views

  let(:admin) { create :user, :admin }
  let(:business) { create :user, :business }

  describe 'success ' do
    it 'create session' do
      post :create, params: { email: admin.email, password: admin.password }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['session_token']).to_not be_nil
    end

    it 'destroy session' do
      session = sign_in user: admin

      delete :destroy, params: { session_token: session.token }

      expect(response.status).to be(200)
      expect(admin.sessions.count).to eq(0)
    end
  end

  describe 'return error' do
    it 'wrong credantials' do
      post :create, params: { email: business.email, password: business.password }

      expect(response.status).to be(400)
      response_data = JSON.parse(response.body)['errors'][0]['message']
      expect(response_data).to eq('Access denied.')
    end

    it 'should render unauthorized if there is no session' do
      delete :destroy, params: { session_token: '' }

      expect(response.status).to be(400)
    end
  end
end

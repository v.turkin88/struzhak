# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Admin::LocalesController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }
  let(:admin) { create :user, :admin }
  let(:business) { create :user, :business }
  let(:artist) { create :user, :artist }

  let(:valid_params) do
    {
      "iso2": "uk",
      "iso3": "ukr",
      "active": true,
      "code": "uk",
      "title": "Укр"
    }
  end

  let(:invalid_params) do
    {
      "iso2": "",
      "iso3": "",
      "active": true,
      "code": "",
      "title": ""
    }
  end

  describe 'with success' do
    before do
      sign_in user: admin
    end

    it 'for create' do
      post :create, params: valid_params

      expect(response.status).to be 200
      expect(Locale.all.count).to be 1
    end

    it 'for index' do
      locale = create :locale, active: true

      get :index
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['locales'].count).to eq(1)
      expect(JSON.parse(response.body)['locales'][0]['iso2']).to eq(locale.iso2)
      expect(JSON.parse(response.body)['locales'][0]['iso3']).to eq(locale.iso3)
    end
  end

  describe 'for error' do
    it 'for create' do
      sign_in user: admin

      post :create, params: invalid_params
      expect(response.status).to be 422
    end

    it 'for business permission' do
      sign_in user: business
      post :create, params: valid_params
      expect(response.status).to be 401
    end

    it 'for artist permission' do
      sign_in user: artist
      post :create, params: valid_params
      expect(response.status).to be 401
    end

    it 'for index' do
      get :index

      expect(response.status).to be 400
    end
  end
end

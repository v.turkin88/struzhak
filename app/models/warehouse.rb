class Warehouse < ApplicationRecord

  # has_one :product_warehouse
  has_and_belongs_to_many :products
  
  validates :name, presence: { message: I18n.t("validations.cant_be_blank") }

  has_attached_file :avatar,
                    styles: { medium: '1000x1000>', thumb: '120x250>' },
                    default_url: '/images/missing.png',
                    path: ":rails_root/public/system/warehouses/:id/avatars/:style/:filename",
                    url: "/system/warehouses/:id/avatars/:style/:filename"

  validates_attachment_size :avatar, less_than: 20.megabytes, unless: Proc.new {|model| model.avatar }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def self.search_query(params)
    warehouses = Warehouse.arel_table

    q = warehouses.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Warehouse.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(warehouses[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(warehouses[:id])
        q.order(warehouses[:id].desc)
      end
    end

    q.where(warehouses[:name].matches("%#{params[:name]}%")) if params[:name].present?
    q.where(warehouses[:user_id].matches("%#{params[:user_id]}%")) if params[:user_id].present?

    q
  end

end

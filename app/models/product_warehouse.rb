class ProductWarehouse < ApplicationRecord

  belongs_to :product
  belongs_to :warehouse
  
  def self.search_query(params)
    product_warehouses = ProductWarehouse.arel_table

    q = product_warehouses.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if ProductWarehouse.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(product_warehouses[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(product_warehouses[:id])
        q.order(product_warehouses[:id].desc)
      end
    end

    q.where(product_warehouses[:product_id].matches("%#{params[:product_id]}%")) if params[:product_id].present?
    # q.where(product_warehouses[:warehouse_id].matches("%#{params[:warehouse_id]}%")) if params[:warehouse_id].present?

    q
  end

end

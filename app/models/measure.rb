class Measure < ApplicationRecord

  has_many :products
  
  def self.search_query(params)
    measures = Measure.arel_table

    q = measures.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Measure.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(measures[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(measures[:id])
        q.order(measures[:id].desc)
      end
    end

    q.where(measures[:name].matches("%#{params[:name]}%")) if params[:name].present?

    q
  end

end

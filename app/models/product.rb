class Product < ApplicationRecord
  
  # has_many :attachments, as: :attachable, dependent: :destroy
  # accepts_nested_attributes_for :attachments, allow_destroy: true

  belongs_to :measure
  # has_many :product_warehouses
  has_and_belongs_to_many :warehouses
  
  validates :name, presence: { message: I18n.t("validations.cant_be_blank") }
  # validates :description, presence: { message: I18n.t("validations.cant_be_blank") }
  # validates :article, presence: { message: I18n.t("validations.cant_be_blank") }

  has_attached_file :avatar,
                    styles: { medium: '1000x1000>', thumb: '120x250>' },
                    default_url: '/images/missing.png',
                    path: ":rails_root/public/system/products/:id/avatars/:style/:filename",
                    url: "/system/products/:id/avatars/:style/:filename"

  validates_attachment_size :avatar, less_than: 20.megabytes, unless: Proc.new {|model| model.avatar }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def self.search_query(params)
    products = Product.arel_table

    q = products.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Product.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(products[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(products[:id])
        q.order(products[:id].desc)
      end
    end

    q.where(products[:name].matches("%#{params[:name]}%")) if params[:name].present?
    q.where(products[:description].matches("%#{params[:description]}%")) if params[:description].present?
    q.where(products[:measure_id].matches("%#{params[:measure_id]}%")) if params[:measure_id].present?

    q
  end

end

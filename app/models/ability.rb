# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.admin?
      can :manage, :all
    elsif user.member?
      member_abilities(user)
    elsif user.manager?
      manager_abilities(user)
    else
      can :index, Role
    end
  end

  def member_abilities(user)
    can :index, Role
  end

  def manager_abilities(user)
    can :index, Role
  end
end

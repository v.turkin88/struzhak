class GeneralLocation < ApplicationRecord
    has_one :user
    has_one :country

    validates :city, presence: true

end
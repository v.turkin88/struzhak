class Admin::SessionsController < Admin::BaseController
  skip_before_action :authenticate_user, except: [:destroy]

  def create
    @user = find_user(params[:email])

    render json: {errors: [{ "message": "Access denied." }]}, status: :bad_request and return unless @user.admin?

    if @user&.authenticate params[:password]
      sign_in user: @user, device_type: params[:device_type], push_token: params[:push_token]

      render json: { session_token: current_session.token }
    else
      render json: {errors: [{message: "Email or password is wrong" }]}, status: :bad_request
    end
  end

  def destroy
    sign_out
    render json: {message: 'Logout successful' }
  end

  def update
    @session = current_session
    if @session.update_attributes update_params
      render json: {message: 'Update successful' }
    else
      render json: {errors: @current_session.errors.map{|k, v| v}}, status: :bad_request
    end
  end
  
  private

  def find_user(attribute)
    user = User.where("email = ? OR nickname = ?", attribute, attribute)
    user.first
  end

  def login_params
    params.permit(:email, :password)
  end

  def logout_params
    params.permit(:session_token)
  end

  def update_params
    allowed_params = params.permit :push_token, :token
    allowed_params
  end
end

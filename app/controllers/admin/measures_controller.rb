class Admin::MeasuresController < Admin::BaseController
  load_and_authorize_resource :measure
  # skip_before_action :authenticate_user


  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Measure.search_query params
    count_query = Measure.search_query params.merge(count: true)

    @measure = Measure.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Measure.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def create
    @measure = Measure.new measure_params

    if @measure.save
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @measure.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @measure.update_attributes measure_params
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @measure.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @measure.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @measure.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def measure_params
    allowed_params = params.permit :id, :name
    
    allowed_params
  end
end

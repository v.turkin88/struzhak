class Admin::RolesController < Admin::BaseController
  load_and_authorize_resource :role

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Role.search_query params
    count_query = Role.search_query params.merge(count: true)

    @roles = Role.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Role.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def show
  end

  def update
    if @role.update_attributes role_params
      render json: { message: "Updated" }
    else
      render json: { validation_errors: @role.errors }
    end
  end

  private

  def role_params
    params.require(:role).permit :title, :description, :to_show
  end
end

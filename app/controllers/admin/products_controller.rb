class Admin::ProductsController < Admin::BaseController
  load_and_authorize_resource :product
  skip_before_action :authenticate_user


  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Product.search_query params
    count_query = Product.search_query params.merge(count: true)

    @products = Product.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Product.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def create
    @product = Product.new product_params

    if @product.save
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @product.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @product.update_attributes product_params
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @product.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @product.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @product.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    @measure = Measure.find_by(id: @product.measure_id)
  end

  private

  def product_params
    allowed_params = params.permit :id, :name, :description, :avatar, :article, :measure_id
                                                     # attachments_attributes: [:id, :file, :attachable_type, :attachment_type]

    allowed_params
  end
end

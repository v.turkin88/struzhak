class Admin::ProductWarehousesController < Admin::BaseController
  # load_and_authorize_resource :product_warehouse
  # skip_before_action :authenticate_user


  def index
    # byebug

    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = ProductWarehouse.search_query params
    count_query = ProductWarehouse.search_query params.merge(count: true)

    @product_warehouse = ProductWarehouse.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = ProductWarehouse.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
      
  end

  def create
    @product_warehouse = ProductWarehouse.new product_warehouse_params

    if @product_warehouse.save
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @product_warehouse.errors }, status: :unprocessable_entity
    end
  end

  def update
    @product_warehouse = ProductWarehouse.find(params[:id])
    if @product_warehouse.update_attributes product_warehouse_update_params
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @product_warehouse.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @product_warehouse = ProductWarehouse.find_by(product_id: params[:id])

    if @product_warehouse.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @product_warehouse.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    @product_warehouse = ProductWarehouse.find_by(product_id: params[:id])
  end

  private

  def product_warehouse_params
    allowed_params = params.permit :id, :warehouse_id, :total_count, :product_id
    allowed_params
  end

  def product_warehouse_update_params
    allowed_params = params.permit :total_count, :id
    allowed_params
  end
end

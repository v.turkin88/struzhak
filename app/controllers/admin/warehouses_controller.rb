class Admin::WarehousesController < Admin::BaseController
  load_and_authorize_resource :warehouse
  skip_before_action :authenticate_user


  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Warehouse.search_query params
    count_query = Warehouse.search_query params.merge(count: true)

    @warehouses = Warehouse.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Warehouse.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def create
    @warehouse = Warehouse.new warehouse_params

    if @warehouse.save
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @warehouse.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @warehouse.update_attributes warehouse_params
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @warehouse.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @warehouse.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @warehouse.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def warehouse_params
    allowed_params = params.permit :id, :name, :avatar, :user_id
                                                     # attachments_attributes: [:id, :file, :attachable_type, :attachment_type]

    allowed_params
  end
end

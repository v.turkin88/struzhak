class LinkThumbnailerWorker
  include Sidekiq::Worker
  sidekiq_options queue: "link_thumbnailer_worker"
  sidekiq_options retry: false

  def perform(id)
    link = UserLink.find(id)
    link.fetch_link
    link.reload
  end
end
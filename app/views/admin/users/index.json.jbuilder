json.users @users.each do |user|
  json.extract! user, :id, :email, :first_name, :last_name, :created_at
  json.role do
    json.extract! user.role, :id, :name
  end
end
json.count @count
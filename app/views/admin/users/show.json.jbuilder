json.user do
  json.extract! @user, :id, :email, :first_name, :last_name, :created_at

  json.role @user.role.name
end

# json.avatar                   @user.avatar.url
# json.avatar_preview           @user.avatar.url :medium
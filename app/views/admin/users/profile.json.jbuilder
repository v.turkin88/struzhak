json.extract! @user, :id, :email, :nickname,:first_name, :last_name, :verified, :created_at

json.role do
  json.extract! @user.role, :id, :name, :title, :description, :order
end

# json.avatar                   @user.avatar.url
# json.avatar_preview           @user.avatar.url :medium
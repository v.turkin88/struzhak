json.role do
  json.extract! @role, :id, :name, :title, :description, :order, :to_show
end
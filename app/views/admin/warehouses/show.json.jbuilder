json.warehouse do
  json.extract! @warehouse, :id, :name, :user_id

  # json.attachments  @warehouse.attachments&.each do |attachment|
  #   json.src         paperclip_url attachment.file
  #   json.url         paperclip_url attachment.file
  #   json.type        attachment.attachment_type
  #   json.original         paperclip_url attachment.file
  #   json.thumbnail        paperclip_url attachment.file
  # end

  json.avatar do
    json.url paperclip_url @warehouse.avatar
  end
end

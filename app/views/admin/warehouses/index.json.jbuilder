json.warehouses @warehouses.each do |warehouse|
  json.extract! warehouse, :id, :name, :user_id, :avatar
end
json.count @count

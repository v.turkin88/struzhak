json.product_warehouse do
  json.extract! @product_warehouse, :id, :total_count

  json.warehouse do
    json.extract! @product_warehouse.warehouse,:id, :name, :avatar
  end
  
  json.product do
    json.extract! @product_warehouse.product, :id, :name, :avatar, :description, :article
  end

  json.measure @product_warehouse.product.measure.name
end

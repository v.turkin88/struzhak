json.product_warehouse do
  # json.product_warehouses @product_warehouses.each do |product_warehouse|
  #   json.extract! product_warehouse, :id, :warehouse_id, :product_id, :total_count
  # end

  json.warehouse do
    json.extract! Warehouse.find(@product_warehouse.first.warehouse_id),:id, :name, :avatar
  end if @product_warehouse.present? 

  prods = Product.where(id: @product_warehouse.pluck(:product_id)) if @product_warehouse.present?

  json.products prods.each do |product|
    json.extract! product, :id, :name, :avatar, :description, :article
    json.count ProductWarehouse.find_by(warehouse_id: @product_warehouse.last.warehouse_id, product_id: product.id).total_count
  end if prods.present?

  json.count @count
end
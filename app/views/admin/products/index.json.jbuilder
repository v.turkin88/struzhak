json.products @products.each do |product|
  json.extract! product, :id, :name, :description, :avatar, :article, :measure_id
  json.measure product.measure.name if product.measure

end
json.count @count

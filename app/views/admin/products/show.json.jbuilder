json.product do
  json.extract! @product, :id, :name, :description, :article
  json.measure product.measure.name if product.measure


  # json.attachments  @product.attachments&.each do |attachment|
  #   json.src         paperclip_url attachment.file
  #   json.url         paperclip_url attachment.file
  #   json.type        attachment.attachment_type
  #   json.original         paperclip_url attachment.file
  #   json.thumbnail        paperclip_url attachment.file
  # end

  json.avatar do
    json.url paperclip_url @product.avatar
  end
end

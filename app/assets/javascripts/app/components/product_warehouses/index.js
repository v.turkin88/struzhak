import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  Checkbox,
  CircularProgress
} from 'material-ui';
import SortingTh from '../common/sorting_th';
import {
  ImageEdit,
  ActionDelete,
  ActionVisibility,
  CommunicationCallMissedOutgoing,
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import {all, activate, destroy} from '../../services/product_warehouses'
import warehouses from '../warehouses';

class ProductWarehouses extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    product_warehouse: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveProductWarehouses();
  }

  _retrieveProductWarehouses = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        product_warehouse: res.product_warehouse,
        count: res.count
      });
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveProductWarehouse);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveProductWarehouse);
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveProductWarehouse)
  };

  _changeSelected = (id, isSelected) => {
    let product_warehouse = {
      id: id,
    };
    activate(product_warehouse)
      .success(res => {
        this._retrieveProductWarehouse()
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        this.setState({
          validationErrors: res.validation_errors
        });
      });
  };

  prepareToAction = (record, action) => {
    this.setState({
      selectedRecord: record,
      showConfirm: true,
      action: action
    })
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveProductWarehouses();
      this.closeConfirm();
    });
  };

  // handleAction = () => {
  //   const { selectedRecord, action } = this.state;
  //   switch (action) {
  //     case 'destroy':
  //       destroy(selectedRecord.id).success(res => {
  //         this._retrieveProductWarehouse();
  //         this.closeConfirm();
  //       });
  //       break;
  //   }
  // };

  render() {
    const { isLoading } = this.props.app.main;
    const { product_warehouse, count, showConfirm } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <h2>{ I18n.t('headers.product_warehouse') }</h2>
            </ul>
          </Col>
        </Row>

        <Row>
          <Col md={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col md={4}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36} />
            <RaisedButton
              href='#/product_warehouse/new'
              className='pull-right'
              primary={true}
              label={ I18n.t('actions.new') }
            />
          </Col>
        </Row>

        <br/>
        <Filters columns={[
          // { label: I18n.t('fields.name'), key: 'name', type: 'string'}
        ]}
                 update={this.updateFilters}
        />
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn/>
              
              <TableHeaderColumn/>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            { product_warehouse.products &&
              product_warehouse.products.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>
                      {
                        product_warehouse.warehouse.avatar ?
                          <img src={product_warehouse.warehouse.avatar} style={{ width: '50px', borderRadius: '50%'}}/>
                          :
                          <img src={'public/images/missing.png'} style={{ width: '40px'}}/>                         
                      }
                    </TableRowColumn>
                    <TableRowColumn>
                      {
                        item.avatar ?
                          <img src={item.avatar} style={{ width: '50px', borderRadius: '50%'}}/>
                          :
                          <img src={'public/images/missing.png'} style={{ width: '40px'}}/>
                      }
                    </TableRowColumn>
      
                    <TableRowColumn>{item.name ? item.name : ''}</TableRowColumn>
                    <TableRowColumn>{item.article ? item.article : ''}</TableRowColumn>
                    <TableRowColumn>{item.count}</TableRowColumn>
                    <TableRowColumn>{item.measure ? item.measure : ''}</TableRowColumn>
                    
                    <TableRowColumn style={{textAlign: 'right'}}>
                      {/* <IconButton onTouchTap={() => location.hash = `#/product_warehouse/${item.id}`}>
                        <ActionVisibility color={palette.primary1Color}/>
                      </IconButton> */}
                      <IconButton onTouchTap={() => location.hash = `#/product_warehouse/${item.id}/edit`}>
                        <ImageEdit color={palette.accent1Color} />
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this, item) }>
                        <ActionDelete color="#c62828"/> 
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Clearfix/>
        <Dialog
        title={ I18n.t('forms.are_you_sure') }
        actions={[
          <FlatButton
            onTouchTap={this.closeConfirm}
            label={ I18n.t('actions.cancel') }
          />,
          <FlatButton
            secondary={true}
            onTouchTap={this.handleDelete}
            label={ I18n.t('actions.confirm') }
          />
        ]}
        modal={false}
        open={showConfirm}
        onRequestClose={this.closeConfirm}
      >
        { I18n.t('forms.you_are_going_to_remove') }
        </Dialog>
      </Paper>
    )
  }
}

ProductWarehouses.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(ProductWarehouses)
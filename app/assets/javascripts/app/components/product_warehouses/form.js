import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Row,
    Col,
    ControlLabel,
    FormGroup,
    Clearfix
} from 'react-bootstrap';
import {
    Paper,
    RaisedButton,
    CircularProgress,
    TextField,
    Toggle,
    TableRowColumn,
    AutoComplete
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/product_warehouses';
import { FormErrorMessage } from '../common/form-error-message.component'
import ReactQuill from 'react-quill';
import ImagesComponent from '../common/images_component';
import Image from '../common/image.component'
import { all as getAllProducts }  from '../../services/products';
import { all as getAllWarehouses }  from '../../services/warehouses';
import product_warehouses from '.';

class ProductWarehouseForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product_warehouse: {
                total_count: 0,
                product: {},
                warehouse: {},
                measure: ''
            },
            warehouses: [],
            products: [],
            validationErrors: {}
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this._retrieveProductWarehouse();
        this._retrieveProductSearch();
        this._retrieveWarehouseSearch();
    }

    _retrieveProductWarehouse = () => {
        const { id } = this.props.params;
        if (!id) {
            return false
        }
        show(id).success(res => {
            this.setState({
                product_warehouse: res.product_warehouse
            })
        })
    };

    _handleChange = (key, value) => {
        const { product_warehouse } = this.state;
        this.setState({
            product_warehouse: {
                ...product_warehouse,
                [key]: value
            },
            validationErrors: {
                ...this.state.validationErrors,
                [key]: null
            }
        }, () => {
            // after state update
        })
    };

    handleChange(value) {
        const { product_warehouse } = this.state;
        this.setState({
            product_warehouse: {
                ...product_warehouse,
                description: value
            }
        })
    }

    _handleSubmit = event => {
        event.preventDefault();
        const { product_warehouse } = this.state;
        upsert(product_warehouse)
            .success(res => {
                location.hash = '#/product_warehouses';
            })
            .progress(value => {
                this.setState({ progress: value })
            })
            .error(res => {
                this.setState({
                    validationErrors: res.validation_errors
                })
            })
    };

    updateMedia = (files, type)=> {
        switch (type){
          case 'photo':
            this.setState({
              product_warehouse: {
                ...this.state.product_warehouse,
                photos: files
              }
            });
            break;
        }
      };

    updateFlag = file => {
        this.setState({
            product_warehouse: {
                ...this.state.product_warehouse,
                flag: file
            }
        })
    };

    _retrieveProductSearch = (value = '') => {
        if (this._productSearchTimeout) {
          clearTimeout(this._productSearchTimeout);
        }
        this._productSearchTimeout = setTimeout( () => {
          getAllProducts({page: 1, per_page: 10}).success(res => {
            this.setState({
              products: res.products
            })
          })
        }, 500);
      };

      _retrieveWarehouseSearch = (value = '') => {
        if (this._warehouseSearchTimeout) {
          clearTimeout(this._warehouseSearchTimeout);
        }
        this._warehouseSearchTimeout = setTimeout( () => {
          getAllWarehouses({page: 1, per_page: 10}).success(res => {
            this.setState({
              warehouses: res.warehouses
            })
          })
        }, 500);
      };

    modules = {
        toolbar: [
            [{ 'header': [1, 2, false] }],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
            ['link', 'image'],
            ['clean']
        ],
    }

    formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
    ]

    render() {
        const { isLoading } = this.props.app.main;
        const { product_warehouse, progress, validationErrors, products, warehouses } = this.state;

        return (
            <Paper style={paperStyle} zDepth={1}>
                <Row>
                    <Col sm={6}>
                        <h2>{product_warehouse.id ? I18n.t('product_warehouse.edit') : I18n.t('product_warehouse.new')}</h2>
                    </Col>
                    <Col sm={6}>
                        <RaisedButton
                            href='#/product_warehouses'
                            className='pull-right'
                            secondary={true}
                            label={I18n.t('actions.back')}
                        />
                    </Col>
                </Row>

                <br />
                <form onSubmit={this._handleSubmit}>

                    <FormGroup>
                        { product_warehouse.id ? 
                          <TextField
                             floatingLabelText={ I18n.t('fields.warehouse_id') }
                             fullWidth={ true }
                             value={ product_warehouse.warehouse.name }
                             disabled={true}
                             
                         />
                        : 
                            <AutoComplete
                                searchText={ product_warehouse.warehouse ? product_warehouse.warehouse.name : ''}
                                errorText={ (validationErrors.warehouse || []).join('. ') }
                                floatingLabelText={I18n.t('fields.warehouse_id')}
                                dataSource={warehouses}
                                fullWidth={true}
                                dataSourceConfig={{text: 'name', value: 'id'}}
                                filter={AutoComplete.caseInsensitiveFilter}
                                openOnFocus={true}
                                onNewRequest={(val) => this._handleChange('warehouse', val)}
                                onUpdateInput={(val) => this._retrieveWarehouseSearch(val) }
                                maxSearchResults={5}
                            />
                        }
                    </FormGroup>

                    <FormGroup>
                      { product_warehouse.id ?
                          <TextField
                             floatingLabelText={ I18n.t('fields.product_id') }
                             fullWidth={ true }
                             value={ product_warehouse.product.name }
                             disabled={true}
                             
                         />
                      : 
                        <AutoComplete
                            searchText={ product_warehouse.product ? product_warehouse.product.name : ''}
                            errorText={ (validationErrors.product || []).join('. ') }
                            floatingLabelText={I18n.t('fields.product_id')}
                            dataSource={products}
                            fullWidth={true}
                            dataSourceConfig={{text: 'name', value: 'id'}}
                            filter={AutoComplete.caseInsensitiveFilter}
                            openOnFocus={true}
                            onNewRequest={(val) => this._handleChange('product', val)}
                            onUpdateInput={(val) => this._retrieveProductSearch(val) }
                            maxSearchResults={5}
                        />
                      }
                    </FormGroup>

                    <FormGroup>
                        <TextField
                            floatingLabelText={ I18n.t('fields.total_count') }
                            fullWidth={ true }
                            value={ product_warehouse.total_count }
                            onChange={ (_, val) => this._handleChange('total_count', val) }
                            errorText={ (validationErrors.key || []).join(', ') }
                        />
                    </FormGroup>

                    <Col sm={4} smOffset={8} className="text-right">
                        <br />
                        <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'}
                            mode="determinate" value={progress} size={36} />
                        <RaisedButton type='submit'
                            primary={true}
                            className='pull-right'
                            label={I18n.t('actions.submit')}
                            disabled={isLoading}
                        />
                    </Col>
                    <Clearfix />
                </form>
            </Paper>
        )
    }
}

export default connect(state => state)(ProductWarehouseForm)

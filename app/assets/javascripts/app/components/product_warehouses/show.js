import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ControlLabel, Row, Col} 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Toggle, Avatar,
} from 'material-ui';

import { paperStyle } from '../common/styles';
import { show } from '../../services/product_warehouses';
import ReactQuill from 'react-quill';

class ProductWarehouses extends Component {
  state = {
    product_warehouse: {
      total_count: '',
      product: '',
      warehouse: ''
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveProductWarehouses();
  }

  _retrieveProductWarehouses = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        product_warhouse: res.product_warhouse,
      })
    });
  };

  render() {
    const {product_warhouse} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('product_warhouse.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/product_warhouses' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.warehouse_id') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {product_warhouse.warehouse_id || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.product_id') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {product_warhouse.product_id || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.total_count') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {product_warhouse.total_count || '-'}</span>
          </Col>
        </Row>
        <hr/>
      </Paper>
    )
  }
}

export default connect(state => state)(ProductWarehouses)

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Row,
    Col,
    ControlLabel,
    FormGroup,
    Clearfix
} from 'react-bootstrap';
import {
    Paper,
    RaisedButton,
    CircularProgress,
    TextField,
    Toggle,
    TableRowColumn,
    AutoComplete
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/measures';
import { FormErrorMessage } from '../common/form-error-message.component'
import ReactQuill from 'react-quill';
import ImagesComponent from '../common/images_component';
import Image from '../common/image.component'

class MeasureForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            measure: {
                name: '',
            },
            validationErrors: {}
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this._retrieveMeasure();
    }

    _retrieveMeasure = () => {
        const { id } = this.props.params;
        if (!id) {
            return false
        }
        show(id).success(res => {
            res.measure.created_at = new Date(res.measure.created_at);
            res.measure.updated_at = new Date(res.measure.updated_at);

            // res.measure.photos = [];
            // res.measure.attachments.map(item => {
            //   switch (item.type){
            //     case 'photo':
            //       res.measure.photos.push(item);
            //       break;
            //   }
            // });

            this.setState({
                measure: res.measure
            })
        })
    };

    _handleChange = (key, value) => {
        const { measure } = this.state;
        this.setState({
            measure: {
                ...measure,
                [key]: value
            },
            validationErrors: {
                ...this.state.validationErrors,
                [key]: null
            }
        }, () => {
            // after state update
        })
    };

    handleChange(value) {
        const { measure } = this.state;
        this.setState({
            measure: {
                ...measure,
                description: value
            }
        })
    }

    _handleSubmit = event => {
        event.preventDefault();
        const { measure } = this.state;
        upsert(measure)
            .success(res => {
                location.hash = '#/measures';
            })
            .progress(value => {
                this.setState({ progress: value })
            })
            .error(res => {
                this.setState({
                    validationErrors: res.validation_errors
                })
            })
    };

    updateMedia = (files, type)=> {
        switch (type){
          case 'photo':
            this.setState({
              measure: {
                ...this.state.measure,
                photos: files
              }
            });
            break;
        }
      };

    updateFlag = file => {
        this.setState({
            measure: {
                ...this.state.measure,
                flag: file
            }
        })
    };

    modules = {
        toolbar: [
            [{ 'header': [1, 2, false] }],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
            ['link', 'image'],
            ['clean']
        ],
    }

    formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
    ]

    render() {
        const { isLoading } = this.props.app.main;
        const { measure, progress, validationErrors } = this.state;

        return (
            <Paper style={paperStyle} zDepth={1}>
                <Row>
                    <Col sm={6}>
                        <h2>{measure.id ? I18n.t('measure.edit') : I18n.t('measure.new')}</h2>
                    </Col>
                    <Col sm={6}>
                        <RaisedButton
                            href='#/measures'
                            className='pull-right'
                            secondary={true}
                            label={I18n.t('actions.back')}
                        />
                    </Col>
                </Row>
                <br />
                <form onSubmit={this._handleSubmit}>
                    <FormGroup>
                        <TextField
                            floatingLabelText={I18n.t('fields.name')}
                            fullWidth={true}
                            value={measure.name}
                            onChange={(_, val) => this._handleChange('name', val)}
                            errorText={(validationErrors.key || []).join(', ')}
                        />
                    </FormGroup>
                    <Col sm={4} smOffset={8} className="text-right">
                        <br />
                        <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'}
                            mode="determinate" value={progress} size={36} />
                        <RaisedButton type='submit'
                            primary={true}
                            className='pull-right'
                            label={I18n.t('actions.submit')}
                            disabled={isLoading}
                        />
                    </Col>
                    <Clearfix />
                </form>
            </Paper>
        )
    }
}

export default connect(state => state)(MeasureForm)

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ControlLabel, Row, Col} 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Toggle, Avatar,
} from 'material-ui';

import { paperStyle } from '../common/styles';
import { show } from '../../services/measures';
import ReactQuill from 'react-quill';

class Measures extends Component {
  state = {
    measure: {
      name: ''
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveMeasures();
  }

  _retrieveMeasures = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        measure: res.measure,
      })
    });
  };

  render() {
    const {measure} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('measure.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/measures' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.name') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {measure.name || '-'}</span>
          </Col>
        </Row>
        <hr/>
      </Paper>
    )
  }
}

export default connect(state => state)(Measures)

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Row,
    Col,
    ControlLabel,
    FormGroup,
    Clearfix
} from 'react-bootstrap';
import {
    Paper,
    RaisedButton,
    CircularProgress,
    TextField,
    Toggle,
    TableRowColumn,
    AutoComplete
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/products';
import { FormErrorMessage } from '../common/form-error-message.component'
import ReactQuill from 'react-quill';
import ImagesComponent from '../common/images_component';
import Image from '../common/image.component'
import { all as getAllMeasures }  from '../../services/measures';

class ProductForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product: {
                name: '',
                description: '',
                avatar: {},
                article: '',
                measure: ''
            },
            measures: [],
            validationErrors: {}
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this._retrieveProduct();
        this._retrieveMeasureSearch();
    }

    _retrieveProduct = () => {
        const { id } = this.props.params;
        if (!id) {
            return false
        }
        show(id).success(res => {
            res.product.created_at = new Date(res.product.created_at);
            res.product.updated_at = new Date(res.product.updated_at);

            // res.product.photos = [];
            // res.product.attachments.map(item => {
            //   switch (item.type){
            //     case 'photo':
            //       res.product.photos.push(item);
            //       break;
            //   }
            // });

            this.setState({
                product: res.product
            })
        })
    };

    _handleChange = (key, value) => {
        const { product } = this.state;
        this.setState({
            product: {
                ...product,
                [key]: value
            },
            validationErrors: {
                ...this.state.validationErrors,
                [key]: null
            }
        }, () => {
            // after state update
        })
    };

    handleChange(value) {
        const { product } = this.state;
        this.setState({
            product: {
                ...product,
                description: value
            }
        })
    }

    _handleSubmit = event => {
        event.preventDefault();
        const { product } = this.state;
        upsert(product)
            .success(res => {
                location.hash = '#/products';
            })
            .progress(value => {
                this.setState({ progress: value })
            })
            .error(res => {
                this.setState({
                    validationErrors: res.validation_errors
                })
            })
    };

    updateMedia = (files, type)=> {
        switch (type){
          case 'photo':
            this.setState({
              product: {
                ...this.state.product,
                photos: files
              }
            });
            break;
        }
      };

    updateFlag = file => {
        this.setState({
            product: {
                ...this.state.product,
                flag: file
            }
        })
    };

    _retrieveMeasureSearch = (value = '') => {
        if (this._measureSearchTimeout) {
          clearTimeout(this._measureSearchTimeout);
        }
        this._measureSearchTimeout = setTimeout( () => {
          getAllMeasures({page: 1, per_page: 10}).success(res => {
            this.setState({
              measures: res.measures
            })
          })
        }, 500);
      };

    modules = {
        toolbar: [
            [{ 'header': [1, 2, false] }],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
            ['link', 'image'],
            ['clean']
        ],
    }

    formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
    ]

    render() {
        const { isLoading } = this.props.app.main;
        const { product, progress, validationErrors, measures } = this.state;

        return (
            <Paper style={paperStyle} zDepth={1}>
                <Row>
                    <Col sm={6}>
                        <h2>{product.id ? I18n.t('product.edit') : I18n.t('product.new')}</h2>
                    </Col>
                    <Col sm={6}>
                        <RaisedButton
                            href='#/products'
                            className='pull-right'
                            secondary={true}
                            label={I18n.t('actions.back')}
                        />
                    </Col>
                </Row>

                <br />
                <form onSubmit={this._handleSubmit}>
                    <FormGroup>
                        <Image value={ product.avatar } onChange={ (val) => this._handleChange('avatar', val) }/>
                        <Clearfix/>
                        <FormErrorMessage errors={ validationErrors.avatar } />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            floatingLabelText={I18n.t('fields.name')}
                            fullWidth={true}
                            value={product.name}
                            onChange={(_, val) => this._handleChange('name', val)}
                            errorText={(validationErrors.key || []).join(', ')}
                        />
                    </FormGroup>
                    <FormGroup>
                        <ReactQuill
                            value={product.description}
                            onChange={this.handleChange}
                            modules={this.modules}
                            formats={this.formats}
                        />
                    </FormGroup>
                   
                    <FormGroup>
                        <TextField
                            floatingLabelText={ I18n.t('fields.article') }
                            fullWidth={ true }
                            value={ product.article }
                            onChange={ (_, val) => this._handleChange('article', val) }
                            errorText={ (validationErrors.key || []).join(', ') }
                        />
                    </FormGroup>

                    <FormGroup>
                        <AutoComplete
                            searchText={ product.measure ? product.measure.name : ''}
                            errorText={ (validationErrors.measure || []).join('. ') }
                            floatingLabelText={I18n.t('product.fields.measure_id')}
                            dataSource={measures}
                            fullWidth={true}
                            dataSourceConfig={{text: 'name', value: 'id'}}
                            filter={AutoComplete.caseInsensitiveFilter}
                            openOnFocus={true}
                            onNewRequest={(val) => this._handleChange('measure', val)}
                            onUpdateInput={(val) => this._retrieveMeasureSearch(val) }
                            maxSearchResults={5}
                        />
                    </FormGroup>

                    <Col sm={4} smOffset={8} className="text-right">
                        <br />
                        <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'}
                            mode="determinate" value={progress} size={36} />
                        <RaisedButton type='submit'
                            primary={true}
                            className='pull-right'
                            label={I18n.t('actions.submit')}
                            disabled={isLoading}
                        />
                    </Col>
                    <Clearfix />
                </form>
            </Paper>
        )
    }
}

export default connect(state => state)(ProductForm)

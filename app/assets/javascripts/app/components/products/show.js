import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ControlLabel, Row, Col} 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Toggle, Avatar,
} from 'material-ui';

import { paperStyle } from '../common/styles';
import { show } from '../../services/products';
import ReactQuill from 'react-quill';

class Products extends Component {
  state = {
    product: {
      name: '',
      description: '',
      avatar:{},
      article: '',
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveProducts();
  }

  _retrieveProducts = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        product: res.product,
      })
    });
  };

  render() {
    const {product} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('product.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/products' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col sm={2}>
            <ControlLabel>{ I18n.t('fields.image') }</ControlLabel>
          </Col>
          <Col sm={10}>
            <Avatar src={ product.avatar ? product.avatar.url : '' } size={ 120 }/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.name') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {product.name || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.article') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {product.article || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={ 12 } md={ 12 } sm={ 12 } xs={ 12 }>
            <ControlLabel>{ I18n.t('fields.description') }</ControlLabel>
            <br/>
            <ReactQuill value={ product.description }
                        readOnly={ true } />
          </Col>
        </Row>
        <hr/>
      </Paper>
    )
  }
}

export default connect(state => state)(Products)

import React, { Component } from 'react';
import { Paper, Chip, Card, CardActions, CardHeader,
         CardMedia, CardTitle, CardText, FlatButton, RadioButton, RadioButtonGroup,
         DatePicker, RaisedButton, Toggle, SelectField, MenuItem, TableRowColumn, TableBody, TableRow,
         IconButton, ActionVisibility
} from 'material-ui';
import { paperStyle } from '../../common/styles';
import { BarChart, Bar, XAxis, YAxis, Tooltip} from 'recharts';
import { all } from '../../../services/piechart_data';
import { Row, Col, ControlLabel } from 'react-bootstrap';
// import {all, activate, destroy} from '../../../services/product_warehouses'


class Blank extends Component {
  state = {
    autoOk: false,
    disableYearSelection: false,
    value: 1,
    from_date: '',
    to_date: '',
    filters: {
      piechart: true,
      value: 1,
      from_date: '',
      to_date: '',
    },
    data: [],
    style: {
      chip: {
        margin: 4,
      },
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      charts: {
        margin: 10
      }
    }
  };

  handleChangeFromDate = (event, date) => {
    this.setState({
      from_date: date,
    });
  };

  handleChangeToDate = (event, date) => {
    this.setState({
      to_date: date,
    });
  };

  handleChange = (event, index, value) => this.setState({
    value: value
  });

  componentWillMount() {
    // this._retrieveData();
  };

  _retrieveData = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        data: res.data,
      })
    })

  };
  updateFilters = () => {
    const { filters } = this.state;
    const { value, from_date, to_date } = this.state;
    filters.value = value;
    filters.from_date = from_date;
    filters.to_date = to_date;
    all(filters).success(res => {
      this.setState({
        data: res.data,
      })
    })
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  // SimpleLineChart = (dataType) =>{
  //   const {style} = this.state;
  //     return (
  //       <div>
  //         <Col xs={20} style={style.charts}>
  //           <Row >
  //             <Col xs={10}>
  //               <Row>
  //                 <LineChart
  //                   width={950}
  //                   height={300}
  //                   data={dataType}>
  //                   <CartesianGrid strokeDasharray="3 3"/>
  //                   <XAxis
  //                     dataKey="name"
  //                     padding={{left: 30, right: 30}}/>
  //                   <YAxis/>
  //                   <Tooltip/>
  //                   <Legend />
  //                   <Line
  //                     type="monotone"
  //                     dataKey="login"
  //                     stroke="#8884d8"
  //                     activeDot={{r: 8}}/>
  //                   <Line
  //                     type="monotone"
  //                     dataKey="registration"
  //                     stroke="#82ca9d"
  //                     activeDot={{r: 8}}/>
  //                   <Line
  //                     type="monotone"
  //                     dataKey="advertisement"
  //                     stroke="#3786c6"
  //                     activeDot={{r: 8}}/>
  //                 </LineChart>
  //               </Row>
  //             </Col>
  //           </Row>
  //         </Col>
  //       </div>
  //     )
  // };

 

  render() {
    const { filters, chipData, style, data} = this.state;

    const userData = [{name: "Admin",  count: data.admin, color: "#ff6262"},
      {name: "Artist", count: data.artist,  color: "#FF8042"},
      {name: "Business", count: data.business,  color: "#3aff27"},
      {name: "Producer", count: data.producer,  color: "#a8141a"},
      {name: "Regular", count: data.regular,  color: "#2069ff"}
    ];


    return (
    <Paper style={paperStyle} zDepth={1}>
      

    </Paper>
    );
  }

}

export default Blank;

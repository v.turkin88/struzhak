import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Row,
    Col,
    ControlLabel,
    FormGroup,
    Clearfix
} from 'react-bootstrap';
import {
    Paper,
    RaisedButton,
    CircularProgress,
    TextField,
    Toggle,
    TableRowColumn,
    AutoComplete
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/warehouses';
import { FormErrorMessage } from '../common/form-error-message.component'
import ReactQuill from 'react-quill';
import ImagesComponent from '../common/images_component';
import Image from '../common/image.component'

class WarehouseForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            warehouse: {
                name: '',
            },
            validationErrors: {}
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this._retrieveWarehouse();
    }

    _retrieveWarehouse = () => {
        const { id } = this.props.params;
        if (!id) {
            return false
        }
        show(id).success(res => {
            res.warehouse.created_at = new Date(res.warehouse.created_at);
            res.warehouse.updated_at = new Date(res.warehouse.updated_at);

            // res.warehouse.photos = [];
            // res.warehouse.attachments.map(item => {
            //   switch (item.type){
            //     case 'photo':
            //       res.warehouse.photos.push(item);
            //       break;
            //   }
            // });

            this.setState({
                warehouse: res.warehouse
            })
        })
    };

    _handleChange = (key, value) => {
        const { warehouse } = this.state;
        this.setState({
            warehouse: {
                ...warehouse,
                [key]: value
            },
            validationErrors: {
                ...this.state.validationErrors,
                [key]: null
            }
        }, () => {
            // after state update
        })
    };

    handleChange(value) {
        const { warehouse } = this.state;
        this.setState({
            warehouse: {
                ...warehouse,
                description: value
            }
        })
    }

    _handleSubmit = event => {
        event.preventDefault();
        const { warehouse } = this.state;
        upsert(warehouse)
            .success(res => {
                location.hash = '#/warehouses';
            })
            .progress(value => {
                this.setState({ progress: value })
            })
            .error(res => {
                this.setState({
                    validationErrors: res.validation_errors
                })
            })
    };

    updateMedia = (files, type)=> {
        switch (type){
          case 'photo':
            this.setState({
              warehouse: {
                ...this.state.warehouse,
                photos: files
              }
            });
            break;
        }
      };

    updateFlag = file => {
        this.setState({
            warehouse: {
                ...this.state.warehouse,
                flag: file
            }
        })
    };

    modules = {
        toolbar: [
            [{ 'header': [1, 2, false] }],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
            ['link', 'image'],
            ['clean']
        ],
    }

    formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
    ]

    render() {
        const { isLoading } = this.props.app.main;
        const { warehouse, progress, validationErrors } = this.state;

        return (
            <Paper style={paperStyle} zDepth={1}>
                <Row>
                    <Col sm={6}>
                        <h2>{warehouse.id ? I18n.t('warehouse.edit') : I18n.t('warehouse.new')}</h2>
                    </Col>
                    <Col sm={6}>
                        <RaisedButton
                            href='#/warehouses'
                            className='pull-right'
                            secondary={true}
                            label={I18n.t('actions.back')}
                        />
                    </Col>
                </Row>

                <br />
                <form onSubmit={this._handleSubmit}>
                    <FormGroup>
                        <Image value={ warehouse.avatar } onChange={ (val) => this._handleChange('avatar', val) }/>
                        <Clearfix/>
                        <FormErrorMessage errors={ validationErrors.avatar } />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            floatingLabelText={I18n.t('fields.name')}
                            fullWidth={true}
                            value={warehouse.name}
                            onChange={(_, val) => this._handleChange('name', val)}
                            errorText={(validationErrors.key || []).join(', ')}
                        />
                    </FormGroup>
             
                    <Col sm={4} smOffset={8} className="text-right">
                        <br />
                        <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'}
                            mode="determinate" value={progress} size={36} />
                        <RaisedButton type='submit'
                            primary={true}
                            className='pull-right'
                            label={I18n.t('actions.submit')}
                            disabled={isLoading}
                        />
                    </Col>
                    <Clearfix />
                </form>
            </Paper>
        )
    }
}

export default connect(state => state)(WarehouseForm)

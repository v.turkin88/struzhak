import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ControlLabel, Row, Col} 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Toggle, Avatar,
} from 'material-ui';

import { paperStyle } from '../common/styles';
import { show } from '../../services/warehouses';
import ReactQuill from 'react-quill';

class Warehouses extends Component {
  state = {
    warehouse: {
      name: '',
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveWarehouses();
  }

  _retrieveWarehouses = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        warehouse: res.warehouse,
      })
    });
  };

  render() {
    const {warehouse} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('warehouse.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/warehouses' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col sm={2}>
            <ControlLabel>{ I18n.t('fields.image') }</ControlLabel>
          </Col>
          <Col sm={10}>
            <Avatar src={ warehouse.avatar ? warehouse.avatar.url : '' } size={ 120 }/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.name') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {warehouse.name || '-'}</span>
          </Col>
        </Row>
        <hr/>
      </Paper>
    )
  }
}

export default connect(state => state)(Warehouses)

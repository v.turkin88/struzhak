import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Avatar
} from 'material-ui';
import {paperStyle} from '../common/styles';
import {show} from '../../services/admin';
import PropTypes from "prop-types";

class User extends Component {
  state = {
    user: {
      first_name: '',
      last_name: '',
      avatar: null
    },
    role: 'admin',
    id: 0,
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveUser();
  };

  componentWillReceiveProps(nextProps) {
    const {role, id} = nextProps.params;
    if (this.state.role === '' || this.state.id === 0) {
      this.setState({
        ...this.state,
        role: role,
        id: id
      })
    }else {
      if (role !== this.state.role || id !== this.state.id) {
        this.setState({
          ...this.state,
          role: role,
          id: id
          }, () => this._retrieveUser()
        );
      }
    }
  }

  _retrieveUser = () => {
    const {id} = this.props.params;
    if (!id) {
      return false
    }
    show(id).success(res => {
      this.setState({
        user: res.user
      })
    })
  };

  handleChange = (key, value) => {
    const {user} = this.state;

    this.setState({
      user: {
        ...user,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    })
  };

  render() {
    const {user} = this.state;
    const { palette } = this.context.muiTheme;
    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{user.first_name} {user.last_name}</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href={`#/users/${user.role}`} className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        {/* <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <Avatar src={user.avatar} size={120}/>
          </Col>
        </Row> */}
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.first_name') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.first_name || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.last_name') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.last_name || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.email') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.email || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.created_at') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.created_at || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Clearfix/>
      </Paper>
    )
  }
}

User.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(User)
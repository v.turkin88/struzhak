export const paperStyle = {
  padding: '2em'
};

export const draft = {
  color: 'rgba(218, 52, 48, 0.84)'
};

export const in_progress = {
  color: '#FFD324'
};

export const delivered = {
  color: 'rgba(40, 181, 9, 0.79)'
};

export const snackBarStyle = {
  success: {
    background: 'rgba(40, 181, 9, 0.79)'
  },
  error: {
    background: 'rgba(218, 52, 48, 0.84)'
  }
};

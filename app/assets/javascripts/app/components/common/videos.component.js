import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import http from '../../services/http';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap'
  },
  item: {
    flex: '1 0 15%',
    height: '120px',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    borderRadius: '4px',
    position: 'relative',
    marginRight: '6px',
    marginBottom: '6px'
  },
  removeIcon: {
    position: 'absolute',
    top: '8px',
    right: '10px',
    color: 'gray',
    fontSize: '20px',
    cursor: 'pointer'
  }
}
export default class Videos extends Component {

  state = {
    page: 1,
    per_page: 10,
    loadedCount: 0
  };

  componentDidMount() {
    this._loadMore()
  }

  _loadMore = () => {
    let {source, onChange, value = []} = this.props;
    let {page, per_page} = this.state;

    if(source){
      source({page, per_page}).then(response => {
        this.setState({
            page: page + 1,
            loadedCount: response.count
          },
          () => {
            onChange([...value, ...response.items]);
          })
      })
    }
  };

  render() {
    const { value = [] } = this.props;
    const { loadedCount } = this.state;

    let hasMore = value.filter((i) => i.id).length < loadedCount;

    return(
      <div
        style={styles.container}
      >
        {
          value.map((video, index) => {
            return (
              video['_destroy']
              ?
              null
              :
              <div
                key={index}
                style={{...styles.item}}
              >
                <video
                  src={`${ video.url }#t=10,20`}
                  controls
                  style={{...styles.item, width: '100%', height: '100%', margin: 0, backgroundColor: 'lightgray'}}
                />
              </div>
            )
          })
        }

        {
          hasMore ?
            <div
              style={{...styles.item, border: "2px dashed #ccc", cursor: 'pointer', display: 'flex', justifyContent: 'center', alignItems: 'center'}}
              onClick={this._loadMore}
            >
              Load 10 more
            </div>
            :
            null
        }

        {/*<div*/}
          {/*style={{...styles.item, border: "2px dashed #ccc", cursor: 'pointer'}}*/}
        {/*>*/}
        {/*</div>*/}
      </div>
    )
  }
}
import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import {
  Table, TableBody,
  TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn,
  IconButton
} from 'material-ui';
import {ActionVisibility} from 'material-ui/svg-icons'
import DropZone from 'react-dropzone';

export default class UsersComponent extends Component {
  state = {
    users: [],
    palette: []
  };

  componentWillReceiveProps(nextProps) {
    const { users, palette } = nextProps;
    if (users) {
      this.setState({
        users: users,
        palette: palette
      })
    }
  }

  render() {

    const { users, palette } = this.state;
    return(
      <Table style={{ tableLayout: 'auto', height:'200px' }} fixedHeader={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>
              { I18n.t('fields.avatar') }
            </TableHeaderColumn>
            <TableHeaderColumn>
              { I18n.t('fields.first_name') }
            </TableHeaderColumn>
            <TableHeaderColumn>
              { I18n.t('fields.last_name') }
            </TableHeaderColumn>
            <TableHeaderColumn>
              { I18n.t('fields.email') }
            </TableHeaderColumn>
            <TableHeaderColumn>
              { I18n.t('fields.phone_number') }
            </TableHeaderColumn>
            <TableHeaderColumn>
              { I18n.t('fields.created_at') }
            </TableHeaderColumn>
            <TableHeaderColumn>
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            users.map(item => {
              return (
                <TableRow key={item.id}>
                  <TableRowColumn><img style={{width: '40px'}} src={item.avatar}/></TableRowColumn>
                  <TableRowColumn>{ item.first_name  }</TableRowColumn>
                  <TableRowColumn>{ item.last_name  }</TableRowColumn>
                  <TableRowColumn>{ item.email  }</TableRowColumn>
                  <TableRowColumn>{ item.phone_number  }</TableRowColumn>
                  <TableRowColumn>{ item.created_at  }</TableRowColumn>
                  <TableRowColumn className='text-right'>
                    <IconButton onTouchTap={() => location.hash = `#/users/${item.role}/${item.id}`}>
                      <ActionVisibility color={palette.primary1Color} />
                    </IconButton>
                  </TableRowColumn>
                </TableRow>
              )
            })
          }
        </TableBody>
      </Table>
    )
  }
}

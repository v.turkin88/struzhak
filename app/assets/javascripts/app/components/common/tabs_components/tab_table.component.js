import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import {
  Table, TableBody,
  TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn,
  IconButton
} from 'material-ui';
import {ActionVisibility} from 'material-ui/svg-icons'
import DropZone from 'react-dropzone';

export default class TabComponent extends Component {
  state = {
    data: [],
    labels: [],
    keys: [],
    not_include_to_sub_array: [],
    sub_array_name: 'user',
    palette: []
  };

  componentWillReceiveProps(nextProps) {
    const { data, labels, keys, not_include_to_sub_array, sub_array_name, palette } = nextProps;
    if (data) {
      this.setState({
        data: data,
        labels: labels,
        keys: keys,
        not_include_to_sub_array: not_include_to_sub_array,
        sub_array_name: sub_array_name,
        palette: palette
      })
    }
  }

  render() {

    const { data, labels, keys, palette, not_include_to_sub_array, sub_array_name } = this.state;
    return(
      <Table style={{ tableLayout: 'auto', height:'200px' }} fixedHeader={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            {labels.map((label, index) => {
              return (
                <TableHeaderColumn key={index}>{label}</TableHeaderColumn>
              )
            }) }
            <TableHeaderColumn>
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            data.map((item, index) => {
              return(
                <TableRow key={index}>
                  {
                    keys.map((key, index) => {
                      return (

                        not_include_to_sub_array.includes(key) ?
                          <TableRowColumn key={index}> {item[key]} </TableRowColumn>
                          :
                          key === 'avatar' ?
                            <TableRowColumn key={Math.random() }><img style={{width: '40px'}}
                                                                     src={item.user.avatar}/></TableRowColumn>
                            :
                          <TableRowColumn key={index}> {item[sub_array_name][key]} </TableRowColumn>


                      )
                    }) }
                  {
                    item[sub_array_name].role !== undefined ?
                    <TableRowColumn className='text-right'>
                    <IconButton
                    onTouchTap={() => location.hash = `#/users/${item[sub_array_name].role}/${item[sub_array_name].id}`}>
                    <ActionVisibility color={palette.primary1Color}/>
                    </IconButton>
                    </TableRowColumn>
                    :
                    null
                  }
                </TableRow>
              )
            })
          }
        </TableBody>
      </Table>
    )
  }
}

import LogoutIcon from './logout'
import DashboardIcon from './dashboard';
import EmailSettingsIcon from './email_settings';
// import GanreIcon from './ganres';
import SystemSubscribeIcon from './system_subscribers';
// import FeedbackIcon from './feedbacks';
import SystemNotificationsIcon from './system_notifications';

export {
  LogoutIcon,
  DashboardIcon,
  EmailSettingsIcon,
  // GanreIcon,
  SystemSubscribeIcon,
  // FeedbackIcon,
  SystemNotificationsIcon
}

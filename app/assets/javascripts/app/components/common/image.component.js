import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import DropZone from 'react-dropzone';

export default class Image extends Component {
  state = {
    model: { url: '' }
  };

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (value) {
      this.setState({
        model: value
      })
    }
  }

  _handleDrop = files => {
    let { onChange } = this.props;
    files = files.filter(file => file['type'].split('/')[0]=='image');
    let images = files.map(file => {return{file: file, url: file.preview}});

    if(images.length > 0){
      onChange(images[0])
    }
  };

  _handleRemove = index => {
    let { onChange } = this.props;
    onChange({})
  };

  render() {

    const { model } = this.state;
    const { contain } = this.props;

    return(
      <div>
        <DropZone
          onDrop={this._handleDrop}
          className='image-preview-dropzone'
          style={{border: `${model ? "0px" : "2px dashed #ccc"}`}}
        >
          <div
            className='image-preview'
            style={{backgroundImage: `url(${model.url})`, height:150, borderRadius:8, backgroundSize: contain ? 'contain' : 'cover'}}
          >
            <div
              className='image-preview text-click'
            >
              {
                model.url ? "" : I18n.t('forms.add_image')
              }
            </div>
          </div>
        </DropZone>
      </div>
    )
  }
}

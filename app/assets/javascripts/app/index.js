import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from "react-router";

import BaseLayout from "./components/layouts/Base";
import DashboardLayout from "./components/layouts";
import injectTapEventPlugin from 'react-tap-event-plugin';
import DashboardOverviewPage from "./components/pages/Dashboard/Overview";
import LoginPage from "./components/pages/Login";
import { store, history } from './create_store';
import { check } from './services/sessions';

// generated components paths

import Admins from './components/admins/index';
import AdminForm from './components/admins/form';
import Admin from './components/admins/show';
import Products from './components/products/index';
import ProductForm from './components/products/form';
import Product from './components/products/show';
import Measures from './components/measures/index';
import MeasureForm from './components/measures/form';
import Measure from './components/measures/show';
import Warehouses from './components/warehouses/index';
import WarehouseForm from './components/warehouses/form';
import Warehouse from './components/warehouses/show';
import ProductWarehouses from './components/product_warehouses/index';
import ProductWarehouseForm from './components/product_warehouses/form';
import ProductWarehouse from './components/product_warehouses/show';

window.onload = function () {
  injectTapEventPlugin();
  ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route name="base" path="/" component={BaseLayout}>
        <Route name="dashboard" path='' component={DashboardLayout} onEnter={check}>
          <IndexRoute name="dashboard.overview" component={DashboardOverviewPage} />
          <Route name='users' path='users/:role' component={Admins} />
          <Route name='new_user' path='users/:role/new' component={AdminForm} />
          <Route name='edit_user' path='users/:role/:id/edit' component={AdminForm} />
          <Route name='show_user' path='users/:role/:id' component={Admin} />

          <Route name='products' path='products' component={Products} />
          <Route name='product' path='product/new' component={ProductForm} />
          <Route name='product' path='product/:id/:edit' component={ProductForm} />
          <Route name='product' path='product/:id' component={Product} />

          <Route name='measures' path='measures' component={Measures} />
          <Route name='measure' path='measure/new' component={MeasureForm} />
          <Route name='measure' path='measure/:id/:edit' component={MeasureForm} />
          <Route name='measure' path='measure/:id' component={Measure} />

          <Route name='warehouses' path='warehouses' component={Warehouses} />
          <Route name='warehouse' path='warehouse/new' component={WarehouseForm} />
          <Route name='warehouse' path='warehouse/:id/:edit' component={WarehouseForm} />
          <Route name='warehouse' path='warehouse/:id' component={Warehouse} />

          <Route name='product_warehouses' path='product_warehouses' component={ProductWarehouses} />
          <Route name='product_warehouse' path='product_warehouse/new' component={ProductWarehouseForm} />
          <Route name='product_warehouse' path='product_warehouse/:id/:edit' component={ProductWarehouseForm} />
          <Route name='product_warehouse' path='product_warehouse/:id' component={Warehouse} />
          
        </Route>
        <Route name="login" path='login' component={LoginPage} />
      </Route>
      <Route path="*" component={LoginPage} />
    </Router>
  </Provider>,
  document.getElementById('content')
  );
};

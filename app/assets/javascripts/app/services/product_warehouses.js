import http from './http';

export function all(filters) {
  let url = '/admin/product_warehouses.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model = {}){
  let body = new FormData();

  body.append('total_count', model.total_count || 0 );

  if (model.warehouse) body.append('warehouse_id', model.warehouse.id);
  if (model.product) body.append('product_id', model.product.id);
    
  if(model.id){
    return http.put({ url:`/admin/product_warehouses/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/product_warehouses', body })
  }
}

export function show(id){
  return http.get({url:`/admin/product_warehouses/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/product_warehouses/${id}`})
}
import http from './http';

export function all(filters) {
  let url = '/admin/products.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model = {}){
  let body = new FormData();

  body.append('description', model.description || '' );
  body.append('name', model.name || '' );
  body.append('article', model.article || '' );

  if (model.measure) body.append('measure_id', model.measure.id);
  
  if(model.avatar && model.avatar.file) {
    body.append('avatar', model.avatar.file );
  }

  
  if(model.id){
    return http.put({ url:`/admin/products/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/products', body })
  }
}

export function show(id){
  return http.get({url:`/admin/products/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/products/${id}`})
}
import http from './http';

export function all(filters) {
  let url = '/admin/warehouses.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model = {}){
  let body = new FormData();

  body.append('name', model.name || '' );
  
  if(model.avatar && model.avatar.file) {
    body.append('avatar', model.avatar.file );
  }

  
  if(model.id){
    return http.put({ url:`/admin/warehouses/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/warehouses', body })
  }
}

export function show(id){
  return http.get({url:`/admin/warehouses/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/warehouses/${id}`})
}
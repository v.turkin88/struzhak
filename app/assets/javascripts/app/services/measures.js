import http from './http';

export function all(filters) {
  let url = '/admin/measures.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model = {}){
  let body = new FormData();

  body.append('name', model.name || '' );
    
  if(model.id){
    return http.put({ url:`/admin/measures/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/measures', body })
  }
}

export function show(id){
  return http.get({url:`/admin/measures/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/measures/${id}`})
}
server  '31.131.18.39',
        port: 22,
        user: 'root',
        roles: [:web, :app, :db], primary: true


# ssh_options[:forward_agent] = true
# set :ssh_options, {:forward_agent => true}
stage = 'staging'
set :application, 'warehouse'
set :repo_url, 'git@gitlab.com:v.turkin88/struzhak.git'
set :branch, 'master'
set :stage, stage

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/warehouse/'

set :linked_files,
    fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs,
    fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache',
                                 'tmp/sockets', 'public/system')

namespace :deploy do
  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
    end
  end
end

Rails.application.routes.draw do

  root to: 'pages#index'

  match 'api/*all' => 'api/base#cors_preflight_check', :constraints => {:method => 'OPTIONS'}, :via => [:options]


  scope '(:locale)' do
    namespace :admin do
      post :login, to: "sessions#create"
      delete :logout, to: "sessions#destroy"
      post :push_token, to: "sessions#update"
      get :profile, to: "users#profile"

      resources :roles, only: [:index, :show, :update, :destroy]
      resources :users, only: [:index, :create, :update, :destroy, :show]
      resources :email_sender, only: [:index, :create]
      resources :products, only: [:index, :create, :update, :destroy, :show]
      resources :measures, only: [:index, :create, :update, :destroy, :show]
      resources :warehouses, only: [:index, :create, :update, :destroy, :show]
      resources :product_warehouses, only: [:index, :create, :update, :destroy, :show]
    end

    resources :sessions, only: [:create] do
      collection do
        delete :destroy
        get :check
      end
    end
  end
end

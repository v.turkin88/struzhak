lock "~> 3.16.0"

set :application, "cleanbreath_admin"
set :repo_url, "git@gitlab.com:viholovko/devit_admin.git"

set :deploy_to, '/var/www/devit_admin'
set :use_sudo, true

set :branch, 'master' #or whichever branch you want to use
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
set :linked_files, fetch(:linked_files, []).push('config/database.yml','config/secrets.yml')

set :ssh_options, {
  forward_agent: true,
}

def seed_roles

  puts 'Start to seed roles ...'

  Role.destroy_all

  roles_attributes = [
    { name: :business, title: "Business", description: "<p>Business role. away not needed, you come to the point, make things clear, add value, you're a content person, you like words. Design is no afterthought, far from it, but it comes in a deserved second.</p>", order: 1},
    { name: :artist, title: "Performer", description: "<p>Performer role. You begin with a text, you sculpt information, you chisel away what's not needed, you come to the point, make things clear, add value, you're a content person, you like words. Design is no afterthought, far from it, but it comes in a deserved second.</p>", order: 2},
    { name: :promoter, title: "Promoter", description: "<p>Promoter role. You begin with a text, you sculpt information, you chisel away what's not needed, you come to the point, make things clear, add value, you're a content person, you like words. Design is no afterthought, far from it, but it comes in a deserved second.</p>", order: 3},
    { name: :regular, title: "Regular", description: "<p>Regular role. You begin with a text, you sculpt information, you chisel away what's not needed, you come to the point, make things clear, add value, you're a content person, you like words. Design is no afterthought, far from it, but it comes in a deserved second.</p>", order: 4},
    { name: :admin, title: "Admin", description: "<p>Admin role. You begin with a text, you sculpt information, you chisel away what's not needed, you come to the point, make things clear, add value, you're a content person, you like words. Design is no afterthought, far from it, but it comes in a deserved second.</p>", order: 0},
  ]

  roles_attributes.each do |role|
    c = Role.create role
  end

  puts 'Roles added'
end

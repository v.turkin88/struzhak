def seed_admin

  puts 'Create admin ...'

  User.create email: 'admin@gmail.com',
              password: 'secret',
              role: Role.get_admin,
              first_name: 'Admin',
              last_name: 'LandCare'
  puts 'Admin created'
end

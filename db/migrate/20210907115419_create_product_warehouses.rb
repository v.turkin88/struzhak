class CreateProductWarehouses < ActiveRecord::Migration[5.1]
  def change
    create_table :product_warehouses do |t|
      t.integer :warehouse_id
      t.integer :product_id
      t.integer :total_count, default: 0
    end
  end
end

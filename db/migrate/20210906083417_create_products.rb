class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string  :name
      t.string  :article
      t.text    :description

      t.timestamps
    end

    add_attachment :products, :avatar

  end
end

class CreateWarehouses < ActiveRecord::Migration[5.1]
  def change
    create_table :warehouses do |t|
      t.integer :user_id, nil: false
      t.string  :name
    end

    add_attachment :warehouses, :avatar

  end
end

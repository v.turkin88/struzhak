# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190111150420) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "attachment_type"
    t.bigint "attachable_id"
    t.string "attachable_type"
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_file_name"
    t.string "file_content_type"
    t.bigint "file_file_size"
    t.datetime "file_updated_at"
    t.index ["attachable_id"], name: "index_attachments_on_attachable_id"
    t.index ["user_id"], name: "index_attachments_on_user_id"
  end

  create_table "contents", force: :cascade do |t|
    t.integer "content_type"
    t.string "key"
    t.text "body"
    t.integer "data_type", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "phone_code"
    t.string "alpha2_code"
    t.string "alpha3_code"
    t.string "numeric_code"
    t.string "flag_file_name"
    t.string "flag_content_type"
    t.bigint "flag_file_size"
    t.datetime "flag_updated_at"
    t.boolean "selected", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.string "content"
    t.string "responce"
    t.string "subject"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faqs", force: :cascade do |t|
    t.string "text"
    t.text "response"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genres", force: :cascade do |t|
    t.string "title"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "privacy_policies", force: :cascade do |t|
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reports", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.string "content"
    t.string "responce"
    t.string "subject"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.text "description"
    t.string "icon_file_name"
    t.string "icon_content_type"
    t.bigint "icon_file_size"
    t.datetime "icon_updated_at"
    t.string "icon_url"
    t.string "background_file_name"
    t.string "background_content_type"
    t.bigint "background_file_size"
    t.datetime "background_updated_at"
    t.string "background_url"
    t.string "background_preview_url"
    t.boolean "to_show", default:   true
    t.integer "order", default: 0
  end

  create_table "sessions", force: :cascade do |t|
    t.string "token"
    t.bigint "user_id"
    t.string "push_token"
    t.integer "device_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "subgenres", force: :cascade do |t|
    t.string "title"
    t.boolean "active", default: false
    t.bigint "genre_id"
    t.index ["genre_id"], name: "index_subgenres_on_genre_id"
  end

  create_table "subgenres_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "subgenre_id", null: false
  end

  create_table "system_settings", force: :cascade do |t|
    t.string "apple_purchase_environment"
    t.string "apple_purchase_password"
    t.string "android_purchase_package_name"
    t.string "android_purchase_product_id"
    t.string "android_purchase_refresh_token"
    t.string "android_purchase_client_id"
    t.string "android_purchase_client_secret"
    t.string "android_purchase_redirect_uri"
  end

  create_table "system_subscribers", force: :cascade do |t|
    t.string "email"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "confirmation_token"
  end

  create_table "term_and_conditions", force: :cascade do |t|
    t.text "body"
  end

  create_table "user_settings", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "system_notification", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_settings_on_user_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "encrypted_password"
    t.string "salt"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.string "nickname"
    t.bigint "role_id"
    t.datetime "last_logged_in"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.bigint "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["role_id"], name: "index_users_on_role_id"
  end

end
